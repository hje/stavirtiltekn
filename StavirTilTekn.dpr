program StavirTilTekn;

uses
  Forms,
  StavirTilTeknUnit in 'StavirTilTeknUnit.pas' {FormStavirTilTekn},
  TeraFunkUnit in '..\TeraFunk\TeraFunkUnit.pas',
  dmMainUnit in 'dmMainUnit.pas' {dmMain: TDataModule},
  FormMainUnit in 'FormMainUnit.pas' {FormMain};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
