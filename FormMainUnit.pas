unit FormMainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmMainUnit,
  StavirTilTeknUnit, Vcl.Buttons, ShellApi, System.ImageList, Vcl.ImgList;

const WM_NOTIFYICON = WM_USER+333;

type
  TFormMain = class(TForm)
    btnStartService: TButton;
    btnSettings: TButton;
    sbOff: TSpeedButton;
    sbOn: TSpeedButton;
    btnEndProgram: TButton;
    ImageList1: TImageList;
    procedure btnStartServiceClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CMClickIcon(var msg: TMessage); message WM_NOTIFYICON;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEndProgramClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.btnEndProgramClick(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TFormMain.btnSettingsClick(Sender: TObject);
var
    FormSettings: TFormStavirTilTekn;
begin
    FormSettings:=TFormStavirTilTekn.Create(self);
    FormSettings.Show;
end;

procedure TFormMain.btnStartServiceClick(Sender: TObject);
begin
    with dmMain do
    begin
        if Timer1.Enabled = True then
        begin
           //ImageList1.GetBitmap(1, SpeedButton1.Glyph) ;
           sbOff.Visible:=True;
           sbOn.Visible:=False;
           Timer1.Enabled:=False;
        end
        else
        begin
           sbOff.Visible:=False;
           sbOn.Visible:=True;
           //ImageList1.GetBitmap(0, SpeedButton1.Glyph) ;
           Timer1.Enabled:=True;
        end;
    end;
end;

procedure TFormMain.CMClickIcon(var msg: TMessage);
begin
    if msg.lparam = WM_LBUTTONDBLCLK then Show;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action:=caNone;
    Hide;
end;

procedure TFormMain.FormCreate(Sender: TObject);
VAR tnid: TNotifyIconData;
    HMainIcon: HICON;
begin
    HMainIcon := LoadIcon(MainInstance, 'MAINICON');
    Shell_NotifyIcon(NIM_DELETE, @tnid);
    tnid.cbSize := sizeof(TNotifyIconData);
    tnid.Wnd := handle;
    tnid.uID := 123;
    tnid.uFlags := NIF_MESSAGE or NIF_ICON or NIF_TIP;
    tnid.uCallbackMessage := WM_NOTIFYICON;
    tnid.hIcon := HMainIcon;
    tnid.szTip := 'StavirTilTekn';
    Shell_NotifyIcon(NIM_ADD, @tnid);

    with dmMain do
    begin
        if Timer1.Enabled = false then
        begin
           sbOff.Visible:=True;
           sbOn.Visible:=False;
        end
        else
        begin
           sbOff.Visible:=False;
           sbOn.Visible:=True;
        end;
    end;
end;

end.
