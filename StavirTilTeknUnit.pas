unit StavirTilTeknUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls,
  DB, ADODB, DBClient, Grids, DBGrids, Mask, DBCtrls, xmldom, XMLIntf,
  msxmldom, XMLDoc, ExtCtrls, ImgList, midaslib, dmMainUnit;

type
  TFormStavirTilTekn = class(TForm)
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    MemoLog: TMemo;
    Memo2: TMemo;
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    BitBtn1: TBitBtn;
    RichEdit1: TRichEdit;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    BitBtn3: TBitBtn;
    TabSheet3: TTabSheet;
    XMLDocument1: TXMLDocument;
    tbsCompanies: TTabSheet;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    DBGrid3: TDBGrid;
    Splitter2: TSplitter;
    DBGrid4: TDBGrid;
    Memo1: TMemo;
    Memo3: TMemo;
    DBNavGlobalSettings: TDBNavigator;
    DBEdit7: TDBEdit;
    Button2: TButton;
    Label8: TLabel;
    Button3: TButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
     procedure InnlesFilar;
     procedure UmsetStavirTilTekn(FilNavn: string);
  public
    sr: TSearchRec;
    BuyersReferenceID: string;
    function FinnKundaNr(XmlNode: IXMLNode): string;
  end;

var
  FormStavirTilTekn: TFormStavirTilTekn;

implementation

uses TeraFunkUnit;

{$R *.dfm}

procedure TFormStavirTilTekn.BitBtn1Click(Sender: TObject);
begin
    InnlesFilar;
end;

procedure TFormStavirTilTekn.Button1Click(Sender: TObject);
begin
    Label1.Caption:=intToStr(ord((Edit1.text[1])));
end;

procedure TFormStavirTilTekn.Button2Click(Sender: TObject);
begin
    dmMain.ProcessInbox;
end;

procedure TFormStavirTilTekn.Button3Click(Sender: TObject);
begin
    if OpenDialog1.Execute then
       dmMain.ImbortCharacterTable(OpenDialog1.FileName);
end;

procedure TFormStavirTilTekn.BitBtn3Click(Sender: TObject);
begin
    //ADOQuery1.Open;
    dmMain.FDQCharTable.Open;
end;

procedure TFormStavirTilTekn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:=caFree;
end;

procedure TFormStavirTilTekn.FormCreate(Sender: TObject);
begin
    PageControl1.ActivePage:= TabSheet3;
end;


procedure TFormStavirTilTekn.UmsetStavirTilTekn(FilNavn: string);
var
  LinjaNr, Tekn: integer;
  OrgStrongur, NewStrongur: AnsiString;
  tempKundaNr: string;
begin
  {  try
        NewStrongur:='NewStrongur';
        ADOQuery1.Open;
        Memo2.Lines.Clear;

        for LinjaNr:=0 to RichEdit1.Lines.Count -1 do
        begin
             NewStrongur:='';
             OrgStrongur:=RichEdit1.Lines[LinjaNr];

             if length(OrgStrongur) > 0 then
             begin
                 For Tekn := 0 to length(OrgStrongur) do
                 begin
                     if OrgStrongur[tekn] <> #0 then
                     begin
                         if (OrgStrongur[tekn] >= #128) and (OrgStrongur[tekn] <= #254) then
                         begin
                            if ADOQuery1.Locate('Nr',ord(OrgStrongur[tekn]),[]) then
                               NewStrongur:=NewStrongur+ADOQuery1htmlcode.AsString
                            else
                               NewStrongur:=NewStrongur+intToStr(ord(OrgStrongur[tekn]));
                         end
                         else
                            NewStrongur:=NewStrongur+OrgStrongur[Tekn];
                     end;
                 end;
             end;
             
             Memo2.Lines.Add(NewStrongur);
        end;

        // Goymn f�lin � �tmappuna
        //KannaFilPath(IncludeTrailingPathDelimiter(cdsUppsetanpathTempMappa.Value));
        KannaFilPath(IncludeTrailingPathDelimiter(cdsUppsetanPathGoymTil.Value));
        Memo2.Lines.SaveToFile(IncludeTrailingPathDelimiter(cdsUppsetanPathGoymTil.Value)+FilNavn);
        //Memo2.Lines.SaveToFile(IncludeTrailingPathDelimiter(cdsUppsetanpathTempMappa.Value)+FilNavn);

        // Goyma � tempmappu fyri s��ani at lesa f�lin inn aftur � ein XML komponent
        // Lesa inn � XML komponent fyri at finna kundanr og s��ani goyma f�lin � �tbakkan undir kundanr.

    except
        on E:exception do
        begin
           LogFile(self,E.message);
           logfile(self,DateTimeToStr(now)+': '+'LinjaNr: '+inttostr(LinjaNr)+' Tekn: '+intToStr(Tekn));
        end;


    end;  }
end;


procedure TFormStavirTilTekn.InnlesFilar;
var
    FilListi: TstringList;
    i: integer;
    ArchivePath: string;
begin
   {  FilListi:=TStringList.Create;
     try
         if FindFirst(IncludeTrailingPathDelimiter(cdsUppsetanPathLesFra.Value)+'*.*',faAnyfile, sr) = 0 then
         try
           repeat
             if (length(sr.Name) > 2) and (uppercase(ExtractFileExt(sr.Name)) = '.XML') then
             begin
                 // Les xml-f�lin inn � ein stringlist
                 FilListi.Add(IncludeTrailingPathDelimiter(cdsUppsetanPathLesFra.Value)+sr.Name)
             end;
           until FindNext(sr) <> 0;
         finally
           FindClose(sr);
         end;

         // Skanna strongin fyri tekn st�rri enn #128
         for i:=0 to FilListi.Count-1 do
         begin
             MemoLog.Lines.Add(FilListi[i]);
             RichEdit1.Lines.LoadFromFile(FilListi[i]);

             UmsetStavirTilTekn(ExtractFileName(FilListi[i]));

             // Flyt f�lin yvir � arkivmappuna
             KannaFilPath(IncludeTrailingPathDelimiter(cdsUppsetanArkivInnlisnirFilar.Value));
             ArchivePath:=umdoypumfilurfinnist(IncludeTrailingPathDelimiter(cdsUppsetanArkivInnlisnirFilar.Value)+ExtractFileName(FilListi[i]));
             MoveFile(Pchar(FilListi[i]), Pchar(ArchivePath));
         end;   
     finally
         FilListi.Free;
     end;   }
end;


function TFormStavirTilTekn.FinnKundaNr(XmlNode: IXMLNode): string;
var
  I: Integer;
  NodeText: string;
  AttrNode: IXMLNode;
begin  
  // skip text nodes and other special cases
  if not (XmlNode.NodeType = ntElement) then
    Exit;
  // add the node itself
  NodeText := XmlNode.NodeName;

  if XmlNode.IsTextElement then
  begin
    NodeText := NodeText + ' = ' + XmlNode.NodeValue;
  end;

  //Memo2.lines.add(NodeText);

  // add attributes
  for I := 0 to xmlNode.AttributeNodes.Count - 1 do
  begin
    AttrNode := xmlNode.AttributeNodes.Nodes[I];
    //Memo2.lines.add('[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');

    //if (AttrNode.NodeName = 'com:BuyerParty') then
    //     showmessage(AttrNode.NodeName+' = '+ AttrNode.Text);
  end;

  // add each child node
  if XmlNode.HasChildNodes then
  begin
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
      begin
        FinnKundaNr (xmlNode.ChildNodes.Nodes [I]);

        if xmlnode.NodeName = 'com:BuyersReferenceID' then
        begin
          //result:=xmlnode.NodeName+': '+XmlNode.NodeValue;
          BuyersReferenceID:=XmlNode.NodeValue;
        end;
      end;
   end;

   result:=BuyersReferenceID;
end;

end.
