object FormStavirTilTekn: TFormStavirTilTekn
  Left = 513
  Top = 318
  Caption = 'Konvertera tekn til HTML kotu'
  ClientHeight = 461
  ClientWidth = 668
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 668
    Height = 461
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    object TabSheet3: TTabSheet
      Caption = 'Uppsetan'
      ImageIndex = 2
      object Label8: TLabel
        Left = 12
        Top = 38
        Width = 35
        Height = 13
        Caption = 'Interval'
      end
      object DBNavGlobalSettings: TDBNavigator
        Left = 12
        Top = 5
        Width = 210
        Height = 25
        DataSource = dmMain.DSoGlobalSettings
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
        TabOrder = 0
      end
      object DBEdit7: TDBEdit
        Left = 12
        Top = 57
        Width = 121
        Height = 21
        DataField = 'INTERVAL'
        DataSource = dmMain.DSoGlobalSettings
        TabOrder = 1
      end
      object Button2: TButton
        Left = 12
        Top = 104
        Width = 75
        Height = 25
        Caption = 'Send'
        TabOrder = 2
        OnClick = Button2Click
      end
    end
    object tbsCompanies: TTabSheet
      Caption = 'Companies'
      ImageIndex = 4
      object Splitter1: TSplitter
        Left = 0
        Top = 193
        Width = 660
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 167
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 273
        Width = 660
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 160
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 660
        Height = 193
        Align = alTop
        DataSource = dmMain.DSoCompany
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NAME'
            Title.Caption = 'Name'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INBOXPATH'
            Title.Caption = 'Inbox Path'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INBOXARCHIVE'
            Title.Caption = 'Inbox Archive'
            Width = 200
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 196
        Width = 660
        Height = 77
        Align = alTop
        DataSource = dmMain.DSoOutbox
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NAME'
            Title.Caption = 'Description'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PATH'
            Title.Caption = 'Outbox Path'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ARCHIVE'
            Title.Caption = 'Outbox Archive'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DEFAULT'
            Title.Caption = 'Default'
            Width = 40
            Visible = True
          end>
      end
      object DBGrid4: TDBGrid
        Left = 0
        Top = 276
        Width = 660
        Height = 157
        Align = alClient
        DataSource = dmMain.DSoOutboxFilter
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DESCRIPTION'
            Title.Caption = 'Description'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GLN'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VTAL'
            Title.Caption = 'V-Tal'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OUTBOXID'
            Title.Caption = 'Outbox'
            Width = 56
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'tekntabell'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 297
        Height = 433
        Align = alLeft
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CHARNUMBER'
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SYMBOL'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HTMLCODE'
            Width = 80
            Visible = True
          end>
      end
      object BitBtn3: TBitBtn
        Left = 303
        Top = 3
        Width = 75
        Height = 25
        Caption = 'Opna'
        TabOrder = 1
        OnClick = BitBtn3Click
      end
      object Button3: TButton
        Left = 534
        Top = 3
        Width = 123
        Height = 25
        Caption = 'Import Character table'
        TabOrder = 2
        OnClick = Button3Click
      end
    end
    object TabSheet1: TTabSheet
      Caption = '.'
      object Label1: TLabel
        Left = 880
        Top = 104
        Width = 32
        Height = 13
        Caption = 'Label1'
      end
      object MemoLog: TMemo
        Left = 256
        Top = 48
        Width = 121
        Height = 89
        TabOrder = 0
      end
      object Memo2: TMemo
        Left = 17
        Top = 107
        Width = 233
        Height = 33
        Lines.Strings = (
          '')
        TabOrder = 1
      end
      object Edit1: TEdit
        Left = 792
        Top = 80
        Width = 121
        Height = 21
        TabOrder = 2
        Text = #248
      end
      object Button1: TButton
        Left = 792
        Top = 48
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 3
        OnClick = Button1Click
      end
      object BitBtn1: TBitBtn
        Left = 16
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Innles XML f'#237'l'
        TabOrder = 4
        OnClick = BitBtn1Click
      end
      object RichEdit1: TRichEdit
        Left = 17
        Top = 47
        Width = 233
        Height = 54
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        WantReturns = False
        WordWrap = False
        Zoom = 100
      end
      object Memo1: TMemo
        Left = 17
        Top = 176
        Width = 256
        Height = 209
        Lines.Strings = (
          'Memo1')
        TabOrder = 6
      end
      object Memo3: TMemo
        Left = 288
        Top = 176
        Width = 185
        Height = 209
        Lines.Strings = (
          'Memo3')
        TabOrder = 7
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 549
    Top = 120
  end
  object DataSource1: TDataSource
    DataSet = dmMain.FDQCharTable
    Left = 340
    Top = 128
  end
  object XMLDocument1: TXMLDocument
    Left = 220
    Top = 147
    DOMVendorDesc = 'MSXML'
  end
end
