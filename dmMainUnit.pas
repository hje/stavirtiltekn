unit dmMainUnit;

interface

uses
  WinApi.Windows,
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, TeraFunkUnit, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TdmMain = class(TDataModule)
    FDConnection1: TFDConnection;
    FDQCompany: TFDQuery;
    FDQGlobalSettings: TFDQuery;
    FDQOutbox: TFDQuery;
    FDQOutBoxFilter: TFDQuery;
    FDQCompanyID: TIntegerField;
    FDQCompanyNAME: TStringField;
    FDQCompanyINBOXPATH: TStringField;
    FDQCompanyINBOXARCHIVE: TStringField;
    FDQGlobalSettingsINTERVAL: TIntegerField;
    FDQOutBoxFilterID: TIntegerField;
    FDQOutBoxFilterGLN: TStringField;
    FDQOutBoxFilterVTAL: TStringField;
    FDQOutBoxFilterOUTBOXID: TIntegerField;
    FDQOutBoxFilterCOMPANYID: TIntegerField;
    FDQOutBoxFilterDESCRIPTION: TStringField;
    DSoGlobalSettings: TDataSource;
    DSoCompany: TDataSource;
    DSoOutboxFilter: TDataSource;
    DSoOutbox: TDataSource;
    FDQCharTable: TFDQuery;
    FDQCharTableCHARNUMBER: TSmallintField;
    FDQCharTableSYMBOL: TStringField;
    FDQCharTableHTMLCODE: TStringField;
    FDQOutboxID: TIntegerField;
    FDQOutboxNAME: TStringField;
    FDQOutboxPATH: TStringField;
    FDQOutboxARCHIVE: TStringField;
    FDQOutboxCOMPANYID: TIntegerField;
    FDQOutboxDEFAULT: TSmallintField;
    Timer1: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure FDQCompanyAfterScroll(DataSet: TDataSet);
    procedure FDQCompanyAfterOpen(DataSet: TDataSet);
    procedure FDQOutboxNewRecord(DataSet: TDataSet);
    procedure FDQOutBoxFilterNewRecord(DataSet: TDataSet);
    procedure FDQOutboxAfterPost(DataSet: TDataSet);
    procedure FDQOutBoxFilterAfterPost(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure ConvertCharToHtmlCode(FileName, OutBox: string);
    procedure SortFiles(CompanyID: integer; Inbox, Archive: string);
  public
    procedure ProcessInbox;
    procedure ImbortCharacterTable(FileName: string);
  end;

var
  dmMain: TdmMain;

implementation

uses
  Vcl.Dialogs;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmMain.ConvertCharToHtmlCode(FileName, OutBox: string);
var
  LinjaNr, Tekn: integer;
  OrgStrongur, NewStrongur: AnsiString;
  tempKundaNr: string;
  OrgInvoice, ConvertedInvoice: TStringList;
begin
   //showmessage(FileName);
    OrgInvoice:=TstringList.Create;
    ConvertedInvoice:=TStringList.Create;
    try
        OrgInvoice.LoadFromFile(FileName);
        try
            NewStrongur:='NewStrongur';
            FDQCharTable.Open;

            for LinjaNr:=0 to OrgInvoice.Count -1 do
            begin
                 NewStrongur:='';
                 OrgStrongur:=OrgInvoice[LinjaNr];

                 if length(OrgStrongur) > 0 then
                 begin
                     For Tekn := 0 to length(OrgStrongur) do
                     begin
                         if OrgStrongur[tekn] <> #0 then
                         begin
                             if (OrgStrongur[tekn] >= #128) and (OrgStrongur[tekn] <= #254) then
                             begin
                                if FDQCharTable.Locate('CHARNUMBER',ord(OrgStrongur[tekn]),[]) then
                                   NewStrongur:=NewStrongur+FDQCharTableHTMLCODE.AsString
                                else
                                   NewStrongur:=NewStrongur+intToStr(ord(OrgStrongur[tekn]));
                             end
                             else
                                NewStrongur:=NewStrongur+OrgStrongur[Tekn];
                         end;
                     end;
                 end;

                 ConvertedInvoice.Add(NewStrongur);
            end;

            // Goymn f�lin � �tmappuna
            KannaFilPath(IncludeTrailingPathDelimiter(OutBox));
            ConvertedInvoice.SaveToFile(IncludeTrailingPathDelimiter(OutBox)+ExtractFileName(FileName),TEncoding.UTF8);
        except
            on E:exception do
            begin
               LogFile(self,E.message);
               logfile(self,DateTimeToStr(now)+': '+'LinjaNr: '+inttostr(LinjaNr)+' Tekn: '+intToStr(Tekn));
            end;
        end;
    finally
        ConvertedInvoice.free;
        OrgInvoice.free;
    end;
end;

procedure TdmMain.SortFiles(CompanyID: integer; Inbox, Archive: string); 
var
    FilListi: TstringList;
    i: integer;
    sr: TSearchRec;
    OutBox: string;
    ArchivePath: string;
begin
     FilListi:=TStringList.Create;
     try
         if FindFirst(IncludeTrailingPathDelimiter(Inbox)+'*.*',faAnyfile, sr) = 0 then
         try
           repeat
             if (length(sr.Name) > 2) and (uppercase(ExtractFileExt(sr.Name)) = '.XML') then
             begin
                 // Les xml-f�lin inn � ein stringlist
                 FilListi.Add(IncludeTrailingPathDelimiter(Inbox)+sr.Name)
             end;
           until FindNext(sr) <> 0;
         finally
           FindClose(sr);
         end;

         FDQOutbox.Close;
         FDQOutbox.ParamByName('COMPANYID').Value:=CompanyID;
         FDQOutbox.Open;

         OutBox:=FDQOutboxPATH.Value;
         ArchivePath:=FDQOutboxARCHIVE.Value;
         // Skanna strongin fyri tekn st�rri enn #128
         for i:=0 to FilListi.Count-1 do
         begin
             //ConvertCharToHtmlCode(ExtractFileName(FilListi[i]), OutBox);
             ConvertCharToHtmlCode(FilListi[i], OutBox);
             // Flyt f�lin yvir � arkivmappuna
             KannaFilPath(IncludeTrailingPathDelimiter(Archive));
             ArchivePath:=umdoypumfilurfinnist(IncludeTrailingPathDelimiter(Archive)+ExtractFileName(FilListi[i]));
             MoveFile(Pchar(FilListi[i]), Pchar(ArchivePath));
         end;
     finally
         FilListi.Free;
     end;
end;


procedure TdmMain.Timer1Timer(Sender: TObject);
begin
    dmMain.ProcessInbox;
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
    if FDConnection1.Connected then
       FDConnection1.Close;

    FDConnection1.Params.Clear;
    FDConnection1.Params.LoadFromFile('StavirTilTekn.ini');

    FDQGlobalSettings.Open;
    FDQCompany.Open;
    Timer1.Interval:=FDQGlobalSettingsINTERVAL.Value*60000;
end;

procedure TdmMain.FDQCompanyAfterOpen(DataSet: TDataSet);
begin
    FDQOutbox.Close;
    FDQOutbox.ParamByName('COMPANYID').Value:=FDQCompanyID.Value;
    FDQOutbox.Open;

    FDQOutBoxFilter.Close;
    FDQOutBoxFilter.ParamByName('COMPANYID').Value:=FDQCompanyID.Value;
    FDQOutBoxFilter.Open;
end;

procedure TdmMain.FDQCompanyAfterScroll(DataSet: TDataSet);
begin
    FDQOutbox.Close;
    FDQOutbox.ParamByName('COMPANYID').Value:=FDQCompanyID.Value;
    FDQOutbox.Open;

    FDQOutBoxFilter.Close;
    FDQOutBoxFilter.ParamByName('COMPANYID').Value:=FDQCompanyID.Value;
    FDQOutBoxFilter.Open;
end;

procedure TdmMain.FDQOutboxAfterPost(DataSet: TDataSet);
begin
    if (FDQOutboxID.Value < 0) then
        FDQOutbox.Refresh;
end;

procedure TdmMain.FDQOutBoxFilterAfterPost(DataSet: TDataSet);
begin
    if (FDQOutBoxFilterID.Value < 0) then
        FDQOutBoxFilter.Refresh;
end;

procedure TdmMain.FDQOutBoxFilterNewRecord(DataSet: TDataSet);
begin
    FDQOutBoxFilterCOMPANYID.Value:=FDQCompanyID.Value;
end;

procedure TdmMain.FDQOutboxNewRecord(DataSet: TDataSet);
begin
    FDQOutboxCOMPANYID.Value:=FDQCompanyID.Value;
end;

procedure TdmMain.ImbortCharacterTable(FileName: string);
var
    strList: TStringlist;
    i: integer;
    tempLine: string;
begin
    strList:=TStringList.Create;
    try
        strList.LoadFromFile(Filename);
        FDQCharTable.Open;

        for i := 0 to strList.Count-1 do
        begin
            tempLine:=strList[i];
            try
                FDQCharTable.Insert;
                FDQCharTableCHARNUMBER.Value:= StrToInt(HeintaTeknSepareraFelt(tempLine,':','"',1));
                FDQCharTableSYMBOL.Value:=HeintaTeknSepareraFelt(tempLine,':','"',2);
                FDQCharTableHTMLCODE.Value:=HeintaTeknSepareraFelt(tempLine,':','"',3);
                FDQCharTable.Post;
            finally
                FDQCharTable.Cancel;
            end;
        end;
    finally
        strlist.Free;
    end;
end;


procedure TdmMain.ProcessInbox;
begin
    LogFile(self,'Start export.');
    FDQCompany.First;

    while not FDQCompany.Eof do
    begin
        LogFile(self,FDQCompanyID.AsString+': '+FDQCompanyNAME.AsString);
        SortFiles(FDQCompanyID.Value, FDQCompanyINBOXPATH.AsString, FDQCompanyINBOXARCHIVE.AsString);
        FDQCompany.Next;
    end;
    LogFile(self,'End export.');
end;

end.
